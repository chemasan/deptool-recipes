Deptool Recipes
============
Recipes for [Deptool](https://gitlab.com/chemasan/deptool)

## TODO
 - [x] Recipe for boost libraies
 - [x] Recipe for Crow
 - [ ] Recipe for CMake
 - [x] Recipe for UnixODBC
 - [ ] Recipe for PostgreeSQL
 - [ ] Recipe for MySQL
 - [x] Recipe for Poco libraries
 - [x] Recipe for yaml-cpp 